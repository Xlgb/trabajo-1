mensajes_Entrada = "programa de generacion de tabla de multiplicar".capitalize()
print(mensajes_Entrada.center(60, "="))

#opcion = 1
opcion = "si"
#while opcion != 0:
while opcion!="no" and opcion!="No":

    tab_Inicio = int(input(chr(27) + "[34;107m" + "Ingrese la tabla inicial :"))
    tan_Final = int(input("Ingrese la tabla Final : "))

    while tan_Final < tab_Inicio:
        print("La tabla de inicio debe ser menor ")
        tab_Inicio = int(input(chr(27) + "[1;107m" +"Ingrese la tabla inicial :"))
        tan_Final = int(input(chr(27) + "[1;31m" +"Ingrese la tabla Final : "))

    rango_Inicio = int(input(chr(27) + "[31;107m" +"Ingrese el rango inicial:"))
    rango_Final = int(input(chr(27) + "[31;107m" +"Ingrese el rango final:"))
    while rango_Inicio > rango_Final:
        print("Rango inicio debe ser menor que el Rango final ")
        rango_Inicio = int(input(chr(27) + "[1;31m" +"Ingrese el rango inicial:"))
        rango_Final = int(input(chr(27) + "[1;31m" +"Ingrese el rango final:"))
    while tab_Inicio<tan_Final:
        for i in range(tab_Inicio, tan_Final + 1):
            if i == 4:
                print(chr(27) + "[1;36m" +"La tabla {0} no la imprimo por que no quiero".format(i))
                continue
            for j in range(rango_Inicio, rango_Final + 1):
                if j == 5:
                    print(chr(27) + "[1;36m" +"Mas de 5 no quiero: ")
                    break
                resultado = i * j
                print(chr(27) + "[1;36m" +"multiplicar %d * %d es igual a : %d" % (i, j, resultado))
                tab_Inicio+=1
    opcion = (input(chr(27) + "[1;107m" +"Desea ejecutar nuevamente el proceso : \n"
                       "Si: Continuar \n"
                       "No: Salir\n"
                       "Ingrese su opcion :"))
else:
    print("Gracias por aprender")
